# Executables (local)
SHELL := /bin/bash
DOCKER_COMP = docker compose

# Docker
PHP_CONT = $(DOCKER_COMP) exec php

# Executables
PHP      = $(PHP_CONT) php
COMPOSER = $(PHP_CONT) composer

# Misc
.DEFAULT_GOAL = help
.PHONY        : help

# App
HOST_USER = `id -un`
HOST_USER_ID = `id -ur`
HOST_USER_GROUP_ID = `id -g`
DEV_USER ?= $(HOST_USER)

COMPOSE_PROJECT_NAME ?= pr
PROJECT_DOMAIN ?= project

## —— 🎵 🐳 The Symfony Docker Makefile 🐳 🎵 ——————————————————————————————————
help: ## Outputs this help screen
	@grep -E '(^[a-zA-Z0-9\./_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}{printf "\033[32m%-30s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

create-env:
	@if [ ! -f ".env.$(DEV_USER).local" ]; then \
  		COMPOSE_PROJECT_NAME=$(shell read -p "Project short name: " token ; echo $${token}); \
  		PROJECT_DOMAIN=$(shell read -p "Project short name: " token ; echo $${token}); \
  		cp .env.dist ".env.$(DEV_USER).local"; \
  		sed -i "s/COMPOSE_PROJECT_NAME=/COMPOSE_PROJECT_NAME=$${COMPOSE_PROJECT_NAME}/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/PROJECT_DOMAIN=/PROJECT_DOMAIN=$${PROJECT_DOMAIN}/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_USER=/CONTAINER_USER=$(HOST_USER)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_UID=/CONTAINER_UID=$(HOST_USER_ID)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_GID=/CONTAINER_GID=$(HOST_USER_GROUP_ID)/g" ".env.$(DEV_USER).local"; \
	fi

create-env-non-interactive:
	@if [ ! -f ".env.$(DEV_USER).local" ]; then \
  		cp .env.dist ".env.$(DEV_USER).local"; \
  		sed -i "s/COMPOSE_PROJECT_NAME=/COMPOSE_PROJECT_NAME=$(COMPOSE_PROJECT_NAME)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/PROJECT_DOMAIN=/PROJECT_DOMAIN=$(PROJECT_DOMAIN)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_USER=/CONTAINER_USER=$(HOST_USER)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_UID=/CONTAINER_UID=$(HOST_USER_ID)/g" ".env.$(DEV_USER).local"; \
  		sed -i "s/CONTAINER_GID=/CONTAINER_GID=$(HOST_USER_GROUP_ID)/g" ".env.$(DEV_USER).local"; \
	fi


## —— Docker 🐳 ————————————————————————————————————————————————————————————————
build: ## Builds the Docker images
	@$(DOCKER_COMP) --env-file .env.$(DEV_USER).local build --pull --no-cache

up: ## Start the docker hub in detached mode (no logs)
	@$(DOCKER_COMP) up --detach

up-dev: ## Start the docker hub in detached mode (no logs)
	$(DOCKER_COMP) --env-file .env.$(DEV_USER).local -f docker-compose.yml -f docker-compose.debug.yml up

up-dev-d: ## Start the docker hub in detached mode (no logs)
	$(DOCKER_COMP) --env-file .env.$(DEV_USER).local -f docker-compose.yml -f docker-compose.debug.yml up -d

down: ## Stop the docker hub
	@$(DOCKER_COMP) down --remove-orphans

logs: ## Show live logs
	@$(DOCKER_COMP) logs --tail=0 --follow

bash: ## Connect to the PHP FPM container
	@$(PHP_CONT) bash

## —— Development 🎵🎵🎵 ———————————————————————————————————————————————————————
start: create-env build up

start-dev: create-env up-dev